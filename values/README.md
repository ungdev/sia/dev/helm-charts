# Values

Ces values sont à insérer dans l'encart values d'ArgoCD, il suffit juste de les modifier ensuite.

## Ajouter des values

* Bien indiquer la source du values.yaml original.
* Utiliser le proxy nexus interne : `docker-io-nexus.uttnetgroup.net`
* Privilégier les variables globales
* Privilégier les charts bitnami : https://github.com/bitnami/charts/blob/master/bitnami
* Configurer correctement le stockage : toujours explicite, et pas de rollingupdate si le stockage ne le permet pas
* Ne pas oublier les backups (sur les PVC et les déploiements de base de données)
* En cas de dépendance à d'autres charts, indiquer l'url vers les values de ce chart et indiquer en haut de bien compléter les values :

```
## https://github.com/bitnami/charts/blob/master/bitnami/wordpress/values.yaml

######
# ATTENTION !!!
# Bien compléter la section mariadb en bas !
######

global:
  imageRegistry: "docker-io-nexus.uttnetgroup.net"
...
...

mariadb:
  ## Tout reprendre depuis https://gitlab.com/ungdev/sia/dev/helm-charts/-/blob/main/values/bitnami-mariadb.yaml (sauf la partie global)

  ...
```
